---
title: Documentation
description: Documentation for Veilid framework and protocol
menu:
  main:
    weight: 4
weight: 1
layout: subpage
---

The documentation is currently a work in progress.

Are you good at writing? <a href="/about-us/community/">We could use your help</a>.

### Documentation Week 

Starting Saturday August 28th, we are having a big documentation work week. The project leads will be getting the information out of their heads and into core documentation.

We're looking to the community to help write out additional documentation, tutorials, and guides. 

Check out the #documentation channel on our [Discord server](/discord).
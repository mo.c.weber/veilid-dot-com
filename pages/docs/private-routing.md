---
title: Private Routing
description: An overview of how private routing works in Veilid
weight: 25
layout: subpage
---

### Private and Safety Routes

<figure>
    <img src="/img/private-and-safety-routes.png" alt="a diagram of routes between points a and b" class="img-fluid">
    <figcaption>
      Veilid Routes are a combination of source and destination private routing. 
       Because no node can trust any other node to pick the whole route, both source and destination must participate.
    </figcaption>
</figure>

### Compiled Routes

<figure>
    <img src="/img/compiled-routes.png" alt="a diagram of routes between points a and b, shown with components" class="img-fluid">
    <figcaption>
      Private Routes are published as a ‘private destination’ and Safety Routes are allocated locally and combined 
      together with a Private Route to form a Compiled Route.
    </figcaption>
</figure>

### Secure Envelopes

<figure>
    <img src="/img/secure-envelopes.png" alt="a diagram how a message is passed from B to A" class="img-fluid">
    <figcaption>
      <p>
        Each node hop only knows about the next one This is similar to onion routing, but assumes that 
        the source is fully in control of the Safety Route and the destination is fully in control of 
        the Private Route.
      </p>
      <p>To zoom in on the details, <a href="/img/secure-envelopes.png">view the image directly</a>.</p>
    </figcaption>
</figure>


### Toward The Future

<div class="focus-text">
  <p>Private routing is a balance of performance and security</p>
  <p>Applications can make use of higher node hop counts if they desire</p>
  <p>Future private routing advancements will be transparent to users</p>
</div>

<ul>
  <li>
    <h4>Per-Hop Payload Keying</h4>
    Ensuring that there is nothing common between packets at each hop will reduce the risk of mass data collection 
    being able to deanonymize routes.
  </li>
  <li>
    <h4>Simplify Directionality</h4>
    Routes are currently bidirectional, but are allocated directionally. 
    We may be able to simplify our allocation mechanism by enforcing bidirectionality. 
    Bidirectional routes are faster, but directional routes could provide more anonymity.
  </li>
  <li>
    <h4>Elimination of Hop Counting</h4>
    Currently the protocol keeps an internal hop count that is not necessary. 
    Efforts should be made to ensure that individual nodes don’t know how far along in a route they are.
  </li>
  <li>
    <h4>Hop Caching</h4>
    Route hop NodeInfo could be cached to save on-the-wire size as well as speed things up.
  </li>
  <li>
    <h4>Increasing Hop Count</h4>
    <p>Currently the default is one hop chosen by the Safety Route, and one hop chosen by the Private Route, which leads to three hops total once compiled.</p>
    <p>It may be important to increase hop count to 2 for users with critical safety needs and to protect from nation-state-level deanonymization where appropriate.</p>
    <p>Existing research (on Tor) suggests that our existing hop count should be sufﬁcient and provide comparable anonymity, but this should be revisited.</p>
  </li>
</ul>

<div class="focus-text">
  <p>IP Privacy means your location is safe too</p>
  <p>Users don’t have to do anything to use it</p>
  <p>No IP address means no tracking, collection, or correlation</p>
</div>

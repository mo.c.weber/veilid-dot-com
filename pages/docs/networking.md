---
title: Networking
description: An overview of the networking used in Veilid
weight: 22
layout: subpage
---

<div class="focus-text">
  <p>All devices are welcome and treated fairly.</p>
  <p>You can use the public Veilid Network or build your own.</p>
  <p>Nodes help each other like mutual aid for connectivity</p>
</div>

All Veilid applications running `veilid-core` are 'nodes', and they are all equal in the eyes of the network. No nodes are 'special'.

Nodes are only limited by the resources they bring and the conﬁguration of the network they are on.

DNS is only used one time during ‘bootstrap'; it is not required though. 

SSL is optional and only for HTTPS Websockets for Veilid Webapps.

### Protocols

Veilid uses UDP, TCP, and Websockets.

Low level protocols supported by Veilid are kept simple, to minimize complications. Everything uses framed RPC operations up
to 64KB in size. Protocol support is extensible and may add WebRTC and other specialized protocols in the future.

### Network Topology

<p><img src="/img/Network-Thumbnail.png" alt="A topology graph for Veilid networks" class="img-fluid"></p>
<p>To zoom in on the details, <a href="/img/Network.png">view the full size image directly</a>.</p>

### Bootstrapping

Bootstrap nodes not 'special' nodes. Any node can bootstrap a Veilid network. Networks can be 'keyed' to keep nodes off that don't have the key.  You can join the ‘big Veilid network' or make your own isolated network.

<ul>
  <li>
    <h4>Ask Bootstraps To ‘Find Self'</h4>
    A single initial DNS TXT record request returns some bootstrap nodes that are known to exist. Those are asked to return nodes that are ‘close' to your own node.
  </li>
  <li>
    <h4>Public Address Detection</h4>
    Nodes are often behind various forms of NAT. Validating one's own public ‘Dial Info' is essential for publishing one's Node Info and answering Find Node requests.
  </li>
  <li>
    <h4>Relay Conﬁguration</h4>
    Low-capability network classes may require the use of Inbound or Outbound relays in order to achieve reachability Nodes help each other out to the best of their ability and incur no penalty for not being able to assist other nodes.
  </li>
  <li>
    <h4>Peer Minimum Refresh </h4>
    Nodes in your routing table are asked to return nodes that are near you as well. Finding nodes close to your own is always harder than ﬁnding nodes far away, so we focus on that with our requests.
  </li>
  <li>
    <h4>Network Class Detection</h4>
    Determining NAT type and what mechanisms can be used to achieve connectivity. Direct connection techniques like reverse connections and UDP hole punching may be inappropriate for some network classes.
  </li>
  <li>
    <h4>Ping Validation</h4>
    Nodes come and go, change address, and are unreliable. Checking routing table nodes for proof-of-life is done with exponential backoff. Nodes are removed from the routing table on a LIFO basis.
  </li>
</ul>

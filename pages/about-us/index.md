---
title: About Us
description: Meet the Veilid Foundation members leading the project as well as contributors. 
menu:
  main:
    weight: 7
layout: subpage
weight: 1
---

<h3>
  Veilid Foundation Members
</h3>


<div class="padded-box">
  <h4 class="padded-box-title">Christien Rioux</h4>
  <p>
    Christien Rioux is the primary author and inventor of Veilid's core technology. 
    A long-time member and figure in the security industry and hacker scene,
    Christien is a member of <a href="https://cultdeadcow.com/members.html">CULT OF THE DEAD COW</a>, 
    and formerly <a href="https://web.archive.org/web/20010118210000/http://www.l0pht.com/">L0pht Heavy Industries</a>,
    the formative hacker think-tank. 
    He has been a company founder and published numerous security tools and advisories over the years, 
    and is also proprietor of the hacker-fashion line <a href="https://www.hack.xxx/">HACK.XXX</a>. 
    Christien is a staunch believer that if you want to change the present you need to build the future.
  </p>
</div>

<div class="padded-box">
  <h4 class="padded-box-title">Katelyn Bowden</h4>
  <p>
    Katelyn Bowden is a hacker, activist, and <a href="https://cultdeadcow.com/members.html">CULT OF THE DEAD COW</a> member, who embraces the human side of hacking and tech. 
    Katelyn has dedicated her life to changing the world for the positive- between her work fighting Non-consensual pornography, and her dedication to educating users on security, she is dedicated to making the internet a safer place for everyone. Her alignment is chaotic good, with a hard emphasis on the chaos.
    She also creates strange furby art and has over 60 dead things on display in her house.
  </p>
</div>

<div class="padded-box">
  <h4 class="padded-box-title">Paul Miller</h4>
  <p>
    Paul Miller is the founder/leader/community organizer of 
    <a href="https://hackers.town">hackers.town</a>, 
    <a href="https://home.oniprojekt.ninja/">Projekt:ONI</a> (Optimistic Nihilists Inc.) organizer and founder, Hacker, Infosec professional, and is a passionate privacy advocate. Paul has worked to show the ways a centralized internet has harmed our culture and the future. He believes you should always be N00bin', and that collectively we can restore the promise of the future the internet once offered us.
  </p>
</div>



### Contributors

Veilid contributors include coders, admins, writers, legal, and more. 

<ul class="col-list">
  <li>TC Johnson</li>
  <li>Jun34u, cDc</li>
  <li>DethVeggie, cDc</li>
  <li>Beka Valentine</li>
  <li>signal9</li>
  <li>Obscure, cDc</li>
  <li>Kirk 'Teknique' Strauser</li>
  <li>Alice 'c0debabe' Rhodes</li>
  <li>Abbie 'antijingoist' Gonzalez</li>
  <li>snowchyld, NsF</li>
  <li>John 'Wrewdison' Whelan</li>
  <li>Robert 'LambdaCalculus' Menes</li>
  <li>Julie Chandler</li>
  <li>Glenn Kurtzrock</li>
  <li>Daniel Meyerson</li>
  <li>CylentKnight</li>
  <li>Robert 'Slugnoodle' Notarfrancesco</li>
</ul>


### Special Thanks

A heart-felt thank you to our families and friends for <strike>putting up with</strike> supporting us.



---
title: Download
description: The code base for the different parts and tools of Veilid
menu:
  main:
    weight: 2
layout: index
---

### Getting Started

Please recursively check out the project using this command

`git clone --recurse-submodules git@gitlab.com:veilid/veilid.git`

Then read the [development](https://gitlab.com/veilid/veilid/-/blob/main/DEVELOPMENT.md) guide to get started.

### About the Code Repo

The main repository is located at https://gitlab.com/veilid/veilid and includes several components
- `veilid-core` - the main Veilid crate: https://crates.io/crates/veilid-core
- `veilid-tools` - misc functions for veilid and friends: https://crates.io/crates/veilid-tools
- `veilid-wasm` - Veilid bindings for webassembly: https://crates.io/crates/veilid-wasm
- `veilid-flutter` - Veilid bindings for Flutter/Dart. Comes with an example program: https://crates.io/crates/veilid-flutter
- Also `veilid-server` and `veilid-cli` source are also in this repo for building headless nodes

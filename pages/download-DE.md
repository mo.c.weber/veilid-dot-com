---
title: Herunterladen
description: Die Code Basis für verschiedene Teile und Tools von Veilid
menu:
  main:
    weight: 2
layout: index
---

### Erste Schritte

Bitte checke dieses Projekt rekursiv aus, in dem Du dieses Kommando benutzt

`git clone --recurse-submodules git@gitlab.com:veilid/veilid.git`

Dann lies das [Entwickler](https://gitlab.com/veilid/veilid/-/blob/main/DEVELOPMENT.md) Handbuch um anzufangen.

### Über das Code Repository

Das Hauptrepository findet sich unter https://gitlab.com/veilid/veilid und enthält die foldenden Komponenten
- `veilid-core` - Der Veilidkern als crate: https://crates.io/crates/veilid-core
- `veilid-tools` - Verschiedene Funktionen für Veilid und seine Freunde: https://crates.io/crates/veilid-tools
- `veilid-wasm` - Veilid bindings für Web Assembly: https://crates.io/crates/veilid-wasm
- `veilid-flutter` - Veilid bindings für Flutter/Dart. Enthält eine Beispiel-Programm: https://crates.io/crates/veilid-flutter
- Auch `veilid-server` und `veilid-cli` Quellcodes sind in diesem Repository, um Nodes im headless mode zu erstellen.

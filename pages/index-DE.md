---
title: Übernimm wieder die Kontrolle
description: Veilid ist ein Open Source Framework für verteilte Applikationen.
menu:
  main:
    weight: 1
layout: index
---


<p class="focus-text highlighted">
  Veilid ist ein
    <span class="highlighter-1">Open Source</span>, 
    <span class="highlighter-2">Peer to Peer</span>, 
    <span class="highlighter-3">Mobile first</span>, 
    <span class="highlighter-4">Netzwerk-orientiertes</span> 
  Applikations Framework.
</p>

<p>Veilid (betont wie "Vay-Lid", aus dem Englischen von "Valid" und "Veiled Identification')</p>
<p>
  Veilid ermöglicht es jedem verteilte private Applikationen zu erstellen. Veilid gibt dem User die Privatsphäre zurück im sich der Datensammlung und dem Online Tracking zu verweigern. Veilid wird mit User Experience, Privatsphäre und Sicherheit als unsere Top Prioritäten gebaut. Es ist Open Source und für jeden zur Benutzung verfügbar oder um damit etwas zu erschaffen.
</p>
<p>
  Veilid geht über bestehendene Privatsphäre-Technologien hinaus und hat das Potenzial die Art und Weise in der wir das Internet benutzen komplett zu ändern. Veilid hat keine Profitbestrebungen, was uns die einzigartige Lage versetzt Ideale zu vertreten ohne kaptialistische Kompromisse eingehen zu müssen.
</p>
<p class="focus-text">
  Wir haben Veild gebaut, weil das Internet als es noch neu und jung war, von uns als offener und scheinbar unendlicher Raum voller Möglichkeiten gesehen wurde.</p>
<p>
  Leider ist das Internet wie wir es heute kennen massiv kommerzialisiert und die Nutzer mit ihren Daten sind zu einer der mistbegehrtesten Ware geworden. Die einzigen Wege sich da gegen zu wehren, das ausgenutzte Produkt von Millardären zu werden is entweder zu technisch für den durchschnittlichen Benutzer oder es bleibt nur die Möglichkeit einfach offline zu gehen.
</p>
<p>
  Wir glauben nicht, dass das fair ist; wir haben immer noch nicht den Traum aufgegeben, dass das gesamte Internet frei sein kann und zugreifbar auch ohne das für seine Privatspähre zu verkaufen.
</p>
<p class="focus-text">
  Wir glauben jeder sollte die Möglichkeit haben Beziehungen zu formen, zu lernen, erstellen und online etwas zu bauen ohne monetariesiert zu werden. 
</p>
<p>
  Mit Veilid ist der Benutzer am Hebel auf eine Art die zugänglich und freundlich ist unabhängig von technischen Fähigkeiten. Wir wollen der Welt das Internet ermöglichen, dass wir schon die ganze Zeit hätten haben sollen.
</p>



<div class="row gx-5 gy-3 mt-5">
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn btn-info btn-lg w-100" href="/framework">Technische Details</a>
  </div>
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn bgv-fuschia btn-lg w-100" href="/chat">VeilidChat</a>
  </div>
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn btn-dark btn-lg w-100" href="/code">Code</a>
  </div>
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn btn-primary btn-lg w-100" href="/discord">Discord</a>
  </div>
</div>

<div class="row gx-5 gy-3 mt-3 justify-content-center">
  <div class="col-12 col-md-6">
  <a class="btn btn-success btn-lg w-100 text-white" href="/donate">Spenden</a>
  </div>
</div>


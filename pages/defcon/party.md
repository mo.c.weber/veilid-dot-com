---
title: Veilid Launch Party
description: There ain't no party like a cDc party; join us to celebrate the launch of the Veilid framework.
weight: 4
layout: subpage
exclude: true
---

<pre>(Begin transmission)</pre>

<div class="row justify-content-center">
  <div class="col-12 col-md-6">
    <img src="/img/break-the-internet.jpg" alt="Veilid Launch Party Flyer, Details Listed Below" class="img-fluid">
  </div>
</div>

<div class="card my-4">
  <div class="card-header">
    <h3 class="card-title m-0 fs-4">
      <span class="text-uppercase">Cult of the Dead Cow</span> breaks the internet!!
    </h3>
  </div>
  <div class="card-body">
    <p>And you can too!</p>
    <ul class="list-inline justify-content-between">
      <li class="list-inline-item">Cash Bar</li>
      <li class="list-inline-item">Book Signings</li>
      <l class="list-inline-item"i>Contests</li>
      <li class="list-inline-item">Live Music</li>
      <li class="list-inline-item">Shenanigans</li>
      <li class="list-inline-item">No Raw Meat!</li>
    </ul>
    <table class="event-table">
      <tr>
        <th scope="row">Where</th>
        <td>
          Forums 105,136
        </td>
      </tr>
      <tr>
        <th scope="row">When</th>
        <td>Friday, August 11. 8pm to Midnight. </td>
      </tr>
      <tr>
        <th scope="row" class="text-nowrap">Dress Code</th>
        <td>
          Recommended (but not required) dress code- y2k 31337 Haxxor threads. 
          Think Zero Cool and Acid Burn meet Max Headroom and Franken Gibe. 
          There is no contest, but the Bovine Mother is watching, so make her proud.
        </td>
      </tr>
      <tr>
        <th scope="row" class="text-nowrap">Schedule</th>
        <td>
          <ul>
            <li><span class="text-nowrap">20:00 - 20:45 &mdash;</span> <span class="text-nowrap">Jackalope</span></li>
            <li><span class="text-nowrap">20:45 - 21:30 &mdash;</span> <span class="text-nowrap">DotorNot</span></li>
            <li><span class="text-nowrap">21:30 - 21:15 &mdash;</span> <span class="text-nowrap">cDc/Veilid</span></li>
            <li><span class="text-nowrap">21:15 - 22:45 &mdash;</span> <span class="text-nowrap">Rocky Rivera &amp; DJ Roza</span></li>
            <li><span class="text-nowrap">22:45 - 23:30 &mdash;</span> <span class="text-nowrap">EVA</span></li>
            <li><span class="text-nowrap">23:30 - Close &mdash;</span> <span class="text-nowrap">DJ McGrew</span></li>
          </ul>
        </td>
      </tr>
    </table>
    <p><a href="https://forum.defcon.org/node/245832">DefCon Forum Post</a></p>
  </div>
</div>

<h3>The herd hath spoken. ooMen.</h3>

<pre>(End transmission)</pre>
